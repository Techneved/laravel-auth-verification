<?php

use Carbon\Carbon;
use Faker\Generator;
use Techneved\LaravelAuthVerification\Models\Verification;

$factory->define(Verification::class, function(Generator $faker){
    return [
        'otp' => $faker->numberBetween(100000, 999999),
        'receiver' => $faker->numberBetween(1000000000,2147483647),
        'updated_at' => Carbon::now()
    ];

});