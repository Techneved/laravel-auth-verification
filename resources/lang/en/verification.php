<?php
return [

    /*
   |--------------------------------------------------------------------------
   | Authentication Language Lines
   |--------------------------------------------------------------------------
   |
   | The following language lines are used during authentication for various
   | messages that we need to display to the user. You are free to modify
   | these language lines according to your application's requirements.
   |
   */


    'invalid_otp' => 'OTP is invalid',
    'expire_otp' => 'OTP has been expired',
    'otp_verified' => 'OTP is successfully verified',

];