<?php

namespace Techneved\LaravelAuthVerification\Tests;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Techneved\LaravelAuthVerification\Models\Verification;

class PhoneOTPTest extends TestCase 
{
    use RefreshDatabase;

    /** @test */
    public function generate_six_digit_otp_number() 
    {
        $otp = Verification::generateOtp();
        $this->assertEquals(6, strlen($otp));
    }

    /** @test */
    public function user_can_generate_otp_for_mobile()
    {
        $data = [
            'receiver' => '1234567890',
        ];

        $response_data = $this->generateVerificationOtp($data);
    
        $this->assertDatabaseHas('verifications', $response_data);
    }

    /** @test */

    public function verification_of_mobile_otp_is_invalid()
    {
        $verification_response = $this->createVerificationOtpFactory();
        
        $data = [
            'otp' => '1231321',
            'receiver' => $verification_response['receiver']
        ];
        
        $response = Verification::verify($data);
        
        $this->assertSame($response,[
            'status' => false,
            'message' => trans('auth-verify::verification.invalid_otp')
        ]);
    }

     /** @test */

     public function verification_of_mobile_otp_has_expired()
     {
         $verification_response = $this->createVerificationOtpFactory(['updated_at' => Carbon::now()->subMinutes(6)]);
         
         $data = [
             'otp' => $verification_response['otp'],
             'receiver' => $verification_response['receiver']
         ];
         
         $response = Verification::verify($data);
         $this->assertSame($response,[
            'status' => false,
            'message' => trans('auth-verify::verification.expire_otp')
        ]);
 
     }

    /** @test */

    public function verification_of_mobile_otp_successfully()
    {
        $verification_response = $this->createVerificationOtpFactory();
        
        $data = [
            'otp' => $verification_response['otp'],
            'receiver' => $verification_response['receiver']
        ];
        
        $response = Verification::verify($data);

        $this->assertSame($response,[
            'status' => true,
            'message' => trans('auth-verify::verification.otp_verified')
        ]);

    }

    /** @test */

    public function after_verification_of_otp_it_should_be_delete()
    {
        $verification_response = $this->createVerificationOtpFactory();
        
        $data = [
            'otp' => $verification_response['otp'],
            'receiver' => $verification_response['receiver']
        ];
        
        $response = Verification::verify($data);

        $this->assertDatabaseMissing('verifications', $data);

    }

    

}