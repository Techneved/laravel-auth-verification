<?php


namespace Techneved\LaravelAuthVerification\Tests\Unit;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Techneved\LaravelAuthVerification\Fake\FakeVerification;
use Techneved\LaravelAuthVerification\Tests\TestCase;

class FakeVerificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function get_otp_using_faker()
    {
        $data = [
            'receiver' => '1234567890',
        ];

        $this->generateVerificationOtp($data);

        $response_otp = FakeVerification::getOTP($data['receiver']);

        $this->assertDatabaseHas('verifications', $response_otp->toArray());


    }
}