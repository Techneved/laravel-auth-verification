<?php


namespace Techneved\LaravelAuthVerification\Tests\Unit;


use Techneved\LaravelAuthVerification\EmailVerification\VerificationKey;
use Techneved\LaravelAuthVerification\Tests\TestCase;

class EmailVerificationTest extends TestCase
{
    /** @test */
    public function generate_key_for_email_verification()
    {
        $email_verification_key = VerificationKey::generate([
            'user_id' => 1,
            'user_type' => 'App/User'
        ]);
        $this->assertNotEmpty($email_verification_key);
    }

    /** @test
     *
     * @throws \Exception
     */

    public function store_verification_key_in_cache()
    {
        $data = [
            'user_id' => 1,
            'user_type' => 'App/User'
        ];

        VerificationKey::generate($data);

        $key = $data['user_id'].'.'.$data['user_type'];
        $this->assertNotNull(cache($key));
    }

    /** @test
     *
     * @throws \Exception
     */
    public function email_verification_link_is_not_verified()
    {
        $data = [
            'user_id' => 1,
            'user_type' => 'App/User'
        ];

        VerificationKey::generate($data);

        $key = $data['user_id'].'.'.$data['user_type'];

        $verified_key_response = VerificationKey::verify($key, 'testing-key');

        $this->assertFalse($verified_key_response);
    }

    /** @test
     *
     * @throws \Exception
     */
    public function email_verification_link_is_verified()
    {
        $data = [
            'user_id' => 1,
            'user_type' => 'App/User'
        ];

        $verification_key = VerificationKey::generate($data);

        $key = $data['user_id'].'.'.$data['user_type'];
        $verified_key_response = VerificationKey::verify($key, $verification_key);

        $this->assertTrue($verified_key_response);
    }

    /** @test
     * @throws \Exception
     */
    public function decrypt_encryption_link()
    {
        $data = [
            'user_id' => 1,
            'user_type' => 'App/User'
        ];

        VerificationKey::generate($data);

        $key = $data['user_id'].'.'.$data['user_type'];

        $this->assertNotNull(cache($key));

        $response = VerificationKey::decryptionKey(cache($key));

        $response_data = explode('.', $response);

        $this->assertEquals($key,  $response_data[0].'.'.$response_data[1]);

    }

    /** @test
     * @throws \Exception
     */
    public function cache_has_been_expired()
    {
        $data = [
            'user_id' => 1,
            'user_type' => 'App/User'
        ];
        $verification_key = VerificationKey::generate($data);
        $verified_key_response = VerificationKey::verify('invalid-cache', $verification_key);
        $this->assertFalse($verified_key_response);

    }
}