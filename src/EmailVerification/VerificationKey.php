<?php


namespace Techneved\LaravelAuthVerification\EmailVerification;


use Illuminate\Support\Str;
use Techneved\LaravelAuthVerification\Exceptions\EncodeDecodeException;


class VerificationKey
{
    /** @var  */
    protected static $verification_key;

    /**
     * Generate key for email verification
     *
     * @param $data
     * @return string
     *
     * @throws \Exception
     */
    public static function generate($data)
    {
        $key = $data['user_id'].'.'.$data['user_type'];

        $encrypt_key = random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES);

        self::$verification_key = self::encryptionKey( $key.'.'.Str::random(10), $encrypt_key);

        self::storeInCache($key, self::$verification_key, $encrypt_key);

        return self::$verification_key.'.'.$encrypt_key;
    }


    /**
     * Store verification code and key
     *
     * @param $key
     * @param $value
     * @param $encryptKey
     * @throws \Exception
     */
    public static function storeInCache($key,  $value, $encryptKey)
    {
        cache([
            $key => [
                'value' => $value,
                'key' => $encryptKey
            ]], 5);
        return;
    }

    /**
     * Verify the email verification link
     *
     * @param $key
     * @param $value
     *
     * @return bool
     *
     * @throws \Exception
     */
    public static function verify($key, $value)
    {
        if (count($data = explode('.',$value)) == 2)
            return self::decryptionKey(cache($key)) === self::decryptionKey(['value' => $data[0], 'key' => $data[1]]) ? true : false;

        return false;
    }

    /**
     * Encrypt the data
     * @param $data
     * @return string
     * @throws \Exception
     */
    public static function encryptionKey($data, $key)
    {
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $ciphertext = sodium_crypto_secretbox($data, $nonce, $key);
        return base64_encode($nonce . $ciphertext);
    }

    /**
     * Decrypt the key of verification code
     *
     * @param $data
     * @return false|string
     */
    public static function decryptionKey($data)
    {
        try {
            if ( !array_key_exists('key', $data) || !array_key_exists('value', $data)) {
                throw new \Exception();
            }
            $nonce = mb_substr(base64_decode($data['value']), 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
            $ciphertext = mb_substr(base64_decode($data['value']), SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');
            return sodium_crypto_secretbox_open($ciphertext, $nonce, $data['key']);
        }
        catch (\Exception $e) {
            return EncodeDecodeException::invalidKey();
        }

    }
}