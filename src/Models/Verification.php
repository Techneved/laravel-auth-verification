<?php

namespace Techneved\LaravelAuthVerification\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{

    /** @var array */
    protected $fillable = ['otp', 'receiver','updated_at'];

    /**
     * Generating the otp
     *
     * @return int|string
     */
    public static function generateOtp()
    {
        return rand(100000, 999999);        
    }

    /**
     * OTP is store in database
     *
     * @param $data
     * @return mixed
     */
    public static function storeUpdate($data) {

        $otp = Verification::generateOtp();
        
        $data['otp'] = $otp;

        $verification = Verification::where('receiver', $data['receiver'])
        ->first();

        if ($verification) {

            return $verification->update($data);
        }

        return Verification::create($data);
    }

    /**
     * Verifying the otp in database
     *
     * @param $data
     * @return array
     */
    public static function verify($data) 
    {
        $verification = Verification::where('receiver',$data['receiver'])->where('otp',$data['otp'])->first();

        if (!$verification) {

            return ['status' => false, 'message' => trans('auth-verify::verification.invalid_otp')];
        }

        if (Carbon::now() > Carbon::parse($verification->updated_at)->addMinutes(5)) {

            return ['status' => false, 'message' => trans('auth-verify::verification.expire_otp')];
        }

        $verification->delete();

        return ['status' => true, 'message' => trans('auth-verify::verification.otp_verified')];
    }

}