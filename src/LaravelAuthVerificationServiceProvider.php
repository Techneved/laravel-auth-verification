<?php

namespace Techneved\LaravelAuthVerification;


use Illuminate\Support\ServiceProvider;

class LaravelAuthVerificationServiceProvider extends ServiceProvider 
{
    public function boot()
    {
        $this->registerResource();

        if ($this->app->runningInConsole()) {

            $this->registerPublishing();
        }
    }

    public function register()
    {
    
    }

    /**
     * Register the package resources
     *
     * @return void
     */
    private function registerResource()
    {

        /** Load migration from package */
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        /** Load translation from package */
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang','auth-verify');
    }
       /**
     * Publishing package's files
     *
     * @return void
     */

    public function registerPublishing()
    {
       
        /** publishing the translation of laravel auth to package*/

        $this->publishes([
            __DIR__.'/../resources/lang/' => resource_path('lang/'),
        ],'auth-verify-translation');

    }

}