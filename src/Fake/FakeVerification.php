<?php
namespace Techneved\LaravelAuthVerification\Fake;

use Techneved\LaravelAuthVerification\Models\Verification;

class FakeVerification
{
    /**
     * get OTP
     * @param $receiver
     * @return mixed
     */
    public static function getOTP($receiver)
    {
        return Verification::where('receiver', $receiver)
            ->first();
    }
}