<?php


namespace Techneved\LaravelAuthVerification\Exceptions;


class EncodeDecodeException extends \RuntimeException
{
    public static function invalidKey()
    {
        return 'Invalid encryption key';
    }
}